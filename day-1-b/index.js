import { report, target } from '../day-1/export-report.js';

const expenses = report.reduce((final, exp1, i) => {
  if (final) return final;

  const remainingReport = report.slice(i + 1);

  const temp = remainingReport.reduce((temp, exp2) => {
    if (temp) return temp;

    const exp3 = target - exp1 - exp2;

    if (report.includes(exp3)) return {
      exp2,
      exp3
    };
  }, undefined);

  if (temp) {
    return exp1 * temp.exp2 * temp.exp3;
  }

}, undefined);

console.log(expenses);