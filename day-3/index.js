import { dirname } from 'path';
import { loadAsMap } from '../utils/reader.js';

const data = await loadAsMap(`${ dirname(import.meta.url) }/input.txt`);
const map = {
  data,
  w: data[0].length,
  h: data.length
}

const getCoord = (map, coords) => {
  return map.data[coords.y][coords.x % map.w]
}

const isTree = (char) => char === '#';
const treeAt = (map, coord) => isTree(getCoord(map, coord));

const calculateTreesOnRoute = (map, stepCoords) => {
  let coords = { x: 0, y: 0 };

  let tally = 0;
  while (coords.y < map.h) {
    if (treeAt(map, coords)) tally++;
    coords.x += stepCoords.x;
    coords.y += stepCoords.y;
  }

  return tally;
}

console.log('part1', calculateTreesOnRoute(map, { x: 3, y: 1 }));

const routes = [
  { x: 1, y: 1 },
  { x: 3, y: 1 },
  { x: 5, y: 1 },
  { x: 7, y: 1 },
  { x: 1, y: 2 },
];

const sum = routes.reduce((total, route) => {
  const count = calculateTreesOnRoute(map, route);

  if (!total) return count;
  return total * count;
}, undefined)

console.log('part2', sum)