import { dirname } from 'path';
import { loadAsNumbers } from '../utils/reader.js';

export const report = await loadAsNumbers(`${ dirname(import.meta.url) }/input.txt`)
export const target = 2020;