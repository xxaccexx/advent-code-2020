import { report, target } from './export-report.js';

const expense = report.reduce((final, exp) => {
  if (final) return final;

  const calc = target - exp;
  if (report.includes(calc)) return calc * exp;
}, undefined)

console.log(expense);