import { readFile } from 'fs/promises';

export const loadInputLines = async path => {
  const t = await readFile(path.replace('file://', ''), 'utf-8');
  return t.split('\n');
}

export const loadAsNumbers = async path => {
  const l = await loadInputLines(path);
  return l.map(x => 
      parseFloat(x)
    )
}

export const loadAsPasswords = async path => {
  const l = await loadInputLines(path);
  return l.map(l => {
    const [rule, pass] = l.split(': ');
    const [range, char] = rule.split(' ');
    const [min, max] = range.split('-');

    return {
      pass,
      char,
      min: parseFloat(min),
      max: parseFloat(max),
    }
  });
}

export const loadAsMap = async path => {
  const l = await loadInputLines(path);
  return l.map(l => l.split(''));
}