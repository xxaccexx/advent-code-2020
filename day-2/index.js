import { dirname } from 'path';
import { loadAsPasswords } from '../utils/reader.js';

const passes = await loadAsPasswords(`${ dirname(import.meta.url) }/input.txt`);

const charRange = passes.reduce((count, auth) => {
  const instances = auth.pass.split('').reduce((tally, c) => {
    if (c == auth.char) return tally + 1;
    return tally;
  }, 0);

  if (instances >= auth.min && instances <= auth.max) return count + 1;
  return count;
}, 0);

console.log('ranged', charRange);

const charPosXOR = passes.reduce((count, auth) => {
  console.log(auth);
  const first = auth.pass[ auth.min - 1 ] == auth.char;
  const second = auth.pass[ auth.max - 1] == auth.char;

  if (first && !second) return count + 1;
  if (!first && second) return count + 1;
  return count;
}, 0);

console.log('indexed', charPosXOR);

